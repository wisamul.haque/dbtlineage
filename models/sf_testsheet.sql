{{ config(materialized='table')}}

with sf_dbt_data as (
    select 
        path from {{source('dev_sources','testsheetforccdpaas')}}
)

select *
from sf_dbt_data