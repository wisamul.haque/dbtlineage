{{ config(materialized='table')}}

with supported_combine_names as (
    select 
        * from {{source('aqsa_source','users')}}
)

select *
from supported_combine_names
