{% macro generate_schema_name(custom_schema_name, node) -%}

    {%- set default_schema = target.schema -%}
    {%- if custom_schema_name is none -%}

        {{ var('schema') }}

    {%- else -%}

        {{ var('schema') }}

    {%- endif -%}

{%- endmacro %}